import React from "react";

// react-bootstrap components
import {
  Badge,
  Button,
  Card,
  Navbar,
  Nav,
  Table,
  Container,
  Row,
  Col,
} from "react-bootstrap";

function TableList() {
  return (
    <>
      <Container fluid>
        <Row>
          <Col md="12">
            <Card className="strpied-tabled-with-hover">
              <Card.Header>
                <Card.Title as="h4">Missions</Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover table-striped">
                  <thead>
                    <tr>
                      <th className="border-0">Nom de l'entreprise</th>
                      <th className="border-0">Travail</th>
                      <th className="border-0">Adresse</th>
                      <th className="border-0">date</th>
                      <th className="border-0">horaire</th>
                      <th className="border-0">Prix/h</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Auchan</td>
                      <td>Agent polyvalant</td>
                      <td>Avenue César / Paris(75015)</td>
                      <td>25/07/2021</td>
                      <td>6h-11h</td>
                      <td>12€</td>
                    </tr>
                    <tr>
                      <td>Le Bar à Fifi</td>
                      <td>Serveur</td>
                      <td>Bord de Marne / Neuilly-sur-Marne(93330)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>10.5€</td>
                    </tr>
                    <tr>
                      <td>Carrefour</td>
                      <td>Caissier</td>
                      <td>15 rue Bel Air / Noisy-le-Grand(93)</td>
                      <td>25/07/2021</td>
                      <td>7-13h</td>
                      <td>11€</td>
                    </tr>
                    <tr>
                      <td>Le Bar à Fifi</td>
                      <td>Serveur</td>
                      <td>Bord de Marne / Neuilly-sur-Marne(93330)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>10.5€</td>
                    </tr>
                    <tr>
                      <td>Auchan</td>
                      <td>Agent polyvalant</td>
                      <td>Avenue César / Paris(75015)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>11€</td>
                    </tr>
                    <tr>
                      <td>Carrefour</td>
                      <td>Caissier</td>
                      <td>15 rue Bel Air / Noisy-le-Grand(93)</td>
                      <td>25/07/2021</td>
                      <td>7-13h</td>
                      <td>11€</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
          <Col md="12">
            <Card className="card-plain table-plain-bg">
              <Card.Header>
                <Card.Title as="h4">Historique des missions</Card.Title>
              </Card.Header>
              <Card.Body className="table-full-width table-responsive px-0">
                <Table className="table-hover">
                  <thead>
                    <tr>
                      <th className="border-0">Nom de l'entreprise</th>
                      <th className="border-0">Travail</th>
                      <th className="border-0">Adresse</th>
                      <th className="border-0">date</th>
                      <th className="border-0">horaire</th>
                      <th className="border-0">Prix/h</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Auchan</td>
                      <td>Agent polyvalant</td>
                      <td>Avenue César / Paris(75015)</td>
                      <td>25/07/2021</td>
                      <td>6h-11h</td>
                      <td>12€</td>
                    </tr>
                    <tr>
                      <td>Le Bar à Fifi</td>
                      <td>Serveur</td>
                      <td>Bord de Marne / Neuilly-sur-Marne(93330)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>10.5€</td>
                    </tr>
                    <tr>
                      <td>Carrefour</td>
                      <td>Caissier</td>
                      <td>15 rue Bel Air / Noisy-le-Grand(93)</td>
                      <td>25/07/2021</td>
                      <td>7-13h</td>
                      <td>11€</td>
                    </tr>
                    <tr>
                      <td>Le Bar à Fifi</td>
                      <td>Serveur</td>
                      <td>Bord de Marne / Neuilly-sur-Marne(93330)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>10.5€</td>
                    </tr>
                    <tr>
                      <td>Auchan</td>
                      <td>Agent polyvalant</td>
                      <td>Avenue César / Paris(75015)</td>
                      <td>25/07/2021</td>
                      <td>18-22h</td>
                      <td>11€</td>
                    </tr>
                    <tr>
                      <td>Carrefour</td>
                      <td>Caissier</td>
                      <td>15 rue Bel Air / Noisy-le-Grand(93)</td>
                      <td>25/07/2021</td>
                      <td>7-13h</td>
                      <td>11€</td>
                    </tr>
                  </tbody>
                </Table>
              </Card.Body>
            </Card>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default TableList;
