import './App.css';
import { BrowserRouter , Switch, Route } from "react-router-dom"
import Home from './pages/Home';
import Header from './component/Navbars/AdminNavbar';
import Header from './view/Notifications';


function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route path="/" exact component={Home} />
      <Route path="/admin/user" exact component={Header} />
      <Route path="/admin/notifications" exact component={Notifications} />
    </Switch>
    </BrowserRouter>

  );
}

export default App;

